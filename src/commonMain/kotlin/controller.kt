package org.example.imageShow

internal expect val imageNames: Array<String>
internal expect val picturesDirPath: String

internal expect fun printImageNames()
