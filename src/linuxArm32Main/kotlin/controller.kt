package org.example.imageShow

import gio2.G_FILE_ATTRIBUTE_STANDARD_NAME
import gio2.G_FILE_QUERY_INFO_NONE
import gio2.G_FILE_TYPE_DIRECTORY
import io.gitlab.guiVista.io.file.File
import kotlinx.cinterop.toKString
import platform.posix.getenv

private const val PNG_EXT = ".png"
private const val TIF_EXT = ".tif"
private const val JPEG_EXT = ".jpeg"
private val picturesDir = File.fromPath("${getenv("HOME")?.toKString()}/Pictures")
internal actual val picturesDirPath = picturesDir.path
internal actual val imageNames by lazy { fetchImageFiles(picturesDir) }

internal actual fun printImageNames() {
    if (imageNames.isNotEmpty()) {
        println("-- Images (from $picturesDirPath) --")
        imageNames.forEach { println("* $it") }
    }
}

private fun fetchImageFiles(dir: File): Array<String> {
    val tmp = mutableListOf<String>()
    val fileType = dir.queryFileType(G_FILE_QUERY_INFO_NONE)
    if (fileType == G_FILE_TYPE_DIRECTORY) {
        val enumerator = dir.enumerateChildren(G_FILE_ATTRIBUTE_STANDARD_NAME, 0u)
        var fileInfo = enumerator?.nextFile()
        while (fileInfo != null) {
            tmp += fileInfo.name
            // It is important to close fileInfo otherwise a memory leak WILL occur.
            fileInfo.close()
            fileInfo = enumerator?.nextFile()
        }
        enumerator?.close()
    }
    return tmp.filter { it.endsWith(PNG_EXT) || it.endsWith(TIF_EXT) || it.endsWith(JPEG_EXT) }.toTypedArray()
}
