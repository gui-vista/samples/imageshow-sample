package org.example.imageShow

import gio2.GApplication
import glib2.gpointer
import gtk3.GtkWindowPosition
import io.gitlab.guiVista.core.fetchEmptyDataPointer
import io.gitlab.guiVista.gui.GuiApplication
import io.gitlab.guiVista.gui.guiApplicationActivateHandler
import io.gitlab.guiVista.io.application.Application
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.staticCFunction

internal lateinit var mainWin: MainWindow

fun main() {
    printImageNames()
    guiApplicationActivateHandler = ::activateApplication
    GuiApplication.create(id = "org.example.image-show").use {
        mainWin = MainWindow(this)
        assignDefaultActivateHandler()
        connectShutdownEvent(staticCFunction(::shutdownApplication), fetchEmptyDataPointer())
        println("Application Status: ${run()}")
    }
}

@Suppress("UNUSED_PARAMETER")
private fun activateApplication(app: Application) {
    // Making use of the CPointer Event Reference technique to gain access to state.
    println("Application ID: ${app.appId}")
    mainWin.createUi {
        title = "Image Show"
        changeDefaultSize(width = 400, height = 400)
        changePosition(GtkWindowPosition.GTK_WIN_POS_CENTER)
        visible = true
    }
}

@Suppress("UNUSED_PARAMETER")
private fun shutdownApplication(app: CPointer<GApplication>, userData: gpointer) {
    println("Shutting down application...")
    mainWin.close()
}
