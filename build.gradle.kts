group = "org.example"
version = "1.0-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.5.30"
}

repositories {
    mavenCentral()
    mavenLocal()
}

kotlin {
    val guiVistaGroupId = "io.gitlab.gui-vista"
    val guiVistaGuiVer = "0.5.0"
    linuxArm32Hfp("linuxArm32") {
        compilations.getByName("main") {
            dependencies {
                cinterops.create("glib2")
                cinterops.create("gio2")
                cinterops.create("gtk3")
                implementation("$guiVistaGroupId:guivista-gui:$guiVistaGuiVer")
            }
        }
        binaries {
            executable("image_show") {
                entryPoint = "org.example.imageShow.main"
            }
        }
    }
    linuxX64 {
        compilations.getByName("main") {
            dependencies {
                cinterops.create("glib2")
                cinterops.create("gio2")
                cinterops.create("gtk3")
                implementation("$guiVistaGroupId:guivista-gui:$guiVistaGuiVer")
            }
        }
        binaries {
            executable("image_show") {
                entryPoint = "org.example.imageShow.main"
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                val kotlinVer = "1.5.30"
                implementation(kotlin("stdlib", kotlinVer))
            }
        }
    }
}
